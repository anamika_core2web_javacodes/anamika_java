//Bruteforce approach
/*Given an array of size N
Count the number of elements having at least one element greater than itself
arr : [2,5,1,4,8,0,8,1,3,8]
N = 10
output : 7

*/
class program2{
	public static void main(String[] args){
		int arr[] = new int[]{2,5,1,4,8,0,8,1,3,8};
		int count = 0;
		for(int i=0; i<arr.length; i++){
			for(int j=0; j<arr.length; j++){
				if(arr[j]>arr[i]){
					count++;
					break;
				}
			}
		}
		System.out.println(count);
	}
}

//Time complexity : O(N^2);
//Space complexity : O(1);



import java.io.*;
class program7{
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the number of rows :");
		int rows= Integer.parseInt(br.readLine());
		for(int i=1; i<=rows; i++){
			int k=1; 
			for(int j=rows-i+1; j>=1; j--){
				if(k%2==0){
					System.out.print((char)(96+j)+" ");
				}
				else{
					System.out.print(j+" ");
				}
				k++;
			}
			System.out.println();
		}
	}
}


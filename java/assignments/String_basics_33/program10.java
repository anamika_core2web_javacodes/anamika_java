//WAP to replace all the character 'a' to 'e'. */

import java.util.*;
class program10{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the string :");
		String str = sc.nextLine();
		char ch[] = str.toCharArray();
		for(int i=0; i<ch.length; i++){
			if(ch[i] == 'a'){
				ch[i] = 'e';
			}
		}
		System.out.println(ch);
	}
}


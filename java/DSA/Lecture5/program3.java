//sum of n numbers : bruteforce approach
import java.io.*;
class program3{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter the number :");
		int num = Integer.parseInt(br.readLine());
		int sum=0;
		for(int i=1; i<=num; i++){
			sum =sum+i;
		}
		System.out.println("The sum of all the numbers is "+sum);
	}
}



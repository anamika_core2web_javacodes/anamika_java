/*A person is storing a date, month and year in variables (write a code to print the date, month and yearand also print total
seconds in the day, month and year. */

class practice6{
	public static void main(String[] args)
	{
		byte date = 21;
		byte month = 12;
		short year = 2023;
		int day_sec = 86400;
		int month_sec = 2628003;
		long year_sec = 31536000;
		System.out.println("date/month/year :"+date+"/"+month+"/"+year);
		System.out.println("seconds in a day :"+day_sec);
		System.out.println("seconds in a month :"+month_sec);
		System.out.println("seconds in an year :"+year_sec);
	}
}
		
		
import java.io.*;
class program9{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the number of rows :");
		int rows = Integer.parseInt(br.readLine());
		for(int i=1; i<=rows; i++){
			for(int sp=1; sp<i; sp++){
				System.out.print("\t");
			}
			int f=1;
			for(int j=1; j<=(rows-i)*2+1; j++){
				if(f==1){
					System.out.print(f+"\t");
					f=0;
				}
				else{
					System.out.print(f+"\t");
					f=1;
				}
			}
			System.out.println();
		}
	}
}


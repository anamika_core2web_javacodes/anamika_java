/* WAP to concat the strings given by the user and count the length of string after concatenation */

import java.util.*;
class program1{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the string 1:");
		String str1 = sc.nextLine();
		System.out.println("Enter the string 2:");
		String str2 = sc.nextLine();
		String str3 = str1+str2;
		System.out.println("the concatenated string is "+str3);
		int count=0;
		int length1 = str1.length();
		int length2 = str2.length();
		int length3= length1+length2;
		char ch[] = new char[length3];
		for(int i=0; i<length3; i++){
			ch[i]=str3.charAt(i);
			count++;
		}
		System.out.println("The length of concatenated string is "+count);

	}
}






/* Take an array from user 
 Add first and last,second and second last and so on in an array 
 */
import java.io.*;
class program7{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the size of an array :");
		int size = Integer.parseInt(br.readLine());
		int arr[] = new int[size];
		
		System.out.println("The elements in the array :");
		for(int i=0; i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		int i=0; 
		int j=arr.length-1;
		int sum=0;

		while(i<=j){
			if(i!=j){

				System.out.print(arr[i]+arr[j]+" ");
			}
			else{
				System.out.print(arr[i]+" ");
			}
			i++;
			j--;
		}
	}
}






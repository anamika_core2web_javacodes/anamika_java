/*
3 3 3 
2 2
1
*/
import java.io.*;
class practical7{
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the number of rows :");
		int rows =Integer.parseInt(br.readLine());
		for(int i=1; i<=rows; i++){
			int num=rows-i+1;
			for(int j=1; j<=rows-i+1; j++){
			
				System.out.print(num+" ");
			}
			System.out.println();
		}
	}
}
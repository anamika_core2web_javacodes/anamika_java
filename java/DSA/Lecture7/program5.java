/* Inplace prefix sum :
 Problem Description:
 - Given an array A of N integers.
 -Construct prefix sum of the array in the given array itself
 -Return an array of integers denoting the prefix sum of the given array 
 problem constraint :
 1<=N<10^5
 1<= A[i] <=10^3
 
 Example input :
 Input 1:
 A = {1,2,3,4,5}
 Input 2:
 A ={4,3,2}

 Example output :
 output 1:
 [1,3,6,10,15]
 output 2:
 [4,7,9]

 */

class program5{
	public static void main(String[] args){
		int arr[] = new int[]{1,2,3,4,5};
		for(int i=1; i<arr.length; i++){
			arr[i]= arr[i-1]+ arr[i];
		}
		for(int i=0 ;i<arr.length; i++){
			System.out.print(arr[i]+" ");
		}
	}
}



//Write the number swapping code without using third variable;
import java.io.*;
class program6{
	static void swapno(int num1,int num2){
		num1 = num1+num2;
		num2 = num1-num2;
		num1 = num1-num2;
		System.out.println("The swapped numbers are "+"\nnum1="+num1+"\nnum2="+num2);
	}
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the numbers :");
		int num1 = Integer.parseInt(br.readLine());
		int num2 = Integer.parseInt(br.readLine());
		swapno(num1,num2);
	}
}







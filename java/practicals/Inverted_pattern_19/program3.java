import java.io.*;
class program3{
        public static void main(String[] args)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter the number of rows :");
                int rows = Integer.parseInt(br.readLine());
		for(int i=1; i<=rows; i++)
		{
			for(int sp=1; sp<i; sp++){
				System.out.print("\t");
			}
			int num=rows-i+1;
			for(int j=(rows-i)*2+1; j>=1; j--){
				System.out.print(num+"\t");
			}
			System.out.println();
		}
		
	}
}


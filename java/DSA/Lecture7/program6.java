/* Count of Elements :
 - Given an array A of N Integers
 - Count the number of elements that have at least one element grater than itself

 problem constraint :
  1 <= N <= 10^5
  1 <=A[i] <= 10^9
   
 Example input :
  Input 1 :
   A =[3,1,2];
  Input 2:
   A = [5,5,2];

   Output 1:
    2
   Output 2:
    1
    */
import java.util.*;

class program6{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the size of an array :");
		int size = sc.nextInt();

		int arr[] = new int[size];
		for(int i=0; i<arr.length; i++){
			arr[i] = sc.nextInt();
		}
		int max = Integer.MIN_VALUE;
		for(int i=0; i<arr.length; i++){
			if(arr[i]>max){
				max = arr[i];
			}
		}
		int count=0;

		for(int i=0; i<arr.length; i++){
			if(arr[i] == max){
				count++;
			}
		}

		System.out.println("The count of the elements that have at least 1 element greater then itself is "+(size-count));
	}
}

		
		

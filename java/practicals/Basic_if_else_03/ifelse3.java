//Write a program to check whether the given character is uppercase or lowercase.

class ifelse3{
	public static void main(String[] args){
		char ch = 'a';
		if(ch>=65 && ch<=90){
			System.out.println("The character is UPPERCASE letter");
		}
		else if(ch>=97 && ch<=122){
			
			System.out.println("The character is lowercase letter");
		}
		else{
			System.out.println("The character is invalid");
		}
	}
}
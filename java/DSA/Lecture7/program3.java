/*Given an array of size N and Q number of queriesd.
 Query contains two parameters (s,e);
 s => StartIndex
 e => end Index
 for all the queries print the sum of all the elements from index s to index e 
arr :[-3,6,2,4,5,2,8,-9,3,1];
N :10;
Q :3
Queries    s    e    sum
   1       1   3     12
   2       2   7     12
   3       1   1     06
  */
import java.util.*;
class program3{
	public static void main(String[] ards){
		Scanner sc = new Scanner(System.in);
		int Q=3;

		
		int arr[] = new int[]{-3,6,2,4,5,2,8,-9,3,1};

		for(int i=0; i<Q; i++){
			System.out.print("Enter start and end Index :");

			int start = sc.nextInt();
			int end = sc.nextInt();
			int sum =0 ;

			for(int j=start; j<=end; j++){
				sum = sum+arr[j];

			}
			System.out.println(sum);
		}
	}
}

			



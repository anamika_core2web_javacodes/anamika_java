/*
3
3 2 
3 2 1
*/
import java.io.*;
class practical2{
	public static void main(String[] args) throws IOException{	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the number of rows :");
		int rows = Integer.parseInt(br.readLine());
		for(int i=1; i<=rows; i++){
			int num=rows;
			for(int j=1; j<=i; j++){
				System.out.print(num+" ");
				num--;
			}
			System.out.println();
		}
	}
}
		
		
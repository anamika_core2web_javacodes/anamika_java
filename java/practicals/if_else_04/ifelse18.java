/* WAP to check whether the students passed with :
first class with distinction, first class, Second class, pass and fail according to his percent grades. 
*/


class ifelse18{
	public static void main(String[] args){
		int marks = 30;

		if(marks >= 80 && marks <= 100){
			System.out.println("First class with distinction");
		}
		else if(marks >= 70 && marks < 75){
			System.out.println("First class");
		}
		else if(marks >= 60 && marks < 70){
			System.out.println("second class");
		}
		else if(marks >= 50 && marks < 60){
			System.out.println("Pass");
		}
		else{
			System.out.println("Fail");
		}
	}
}
		
		

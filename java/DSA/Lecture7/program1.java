/*Take the array of size N
 Take start and end index from the user and print their addition
 */
import java.util.*;
class PrintAddition{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the size of an array :");
		int size = sc.nextInt();
		int arr[] = new int[size];
		for(int i=0; i<arr.length; i++){
			arr[i] = sc.nextInt();
		}

		System.out.print("Enter the StartIndex and EndIndex :");
		int startIndex = sc.nextInt();
		int endIndex = sc.nextInt();
		int sum = 0;

		for(int i=startIndex; i<=endIndex; i++){
			sum = sum+arr[i];
		}
		System.out.println(sum);
	}
}


	

//WAP to check whether the given character is a vowel or consonant.
class ifelse15{
	public static void main(String[] args){
		char ch='D';
		if(ch=='a'||ch=='A'||ch=='e'||ch=='E'||ch=='i'||ch=='I'||ch=='o'||ch=='O'||ch=='u'||ch=='U'){
			System.out.println(ch+" is a vowel");
		}
		else{
			System.out.println(ch+" is a consonant");
		}
	}
}


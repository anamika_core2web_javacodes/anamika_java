//WAP to print the table of 14 in reverse order from 140

class forloop8{
	public static void main(String[] args){
		for(int i=140; i>=0; i--){
			if(i%14 == 0){
				System.out.println(i);
			}
		}
	}
}
import java.io.*;
class program8{
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the number of rows :");
		int rows = Integer.parseInt(br.readLine());
		for(int i=1; i<=rows; i++){
			for(int j=rows-i+1; j>=1; j--){
				if(i%2==0){
					System.out.print((char)(64+j)+" ");
				}
				else{
					System.out.print(j+" ");
				}
			}
			System.out.println();
		}
	}
}

/* WAP to check if the string is empty if not then print the last character of the string.Take the string input from the user. */

import java.util.*;
class program9{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the string :");
		String str = new String(sc.nextLine());
		int len=str.length();
		int count=0;

		while(len>0){
			count++;
			len--;
		}
		if(count==0){
			System.out.println("String is empty ");
		}
		else{
			System.out.println(str.charAt(count-1));
		}
	}
}







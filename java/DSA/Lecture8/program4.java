/*
 Given an integer array of size N
 Build an array rightmax of size N
 i contains the maximum for the index i to index N-1
 */
class program4{
	public static void main(String[] args){
		int arr[] = new int[]{-3,6,2,4,5,2,8,-9,3,1};
		int rightmax[] = new int[arr.length];
		rightmax[arr.length-1] = arr[arr.length-1];

	       for(int i=arr.length-2; i>=0; i--){
		       if(arr[i]>rightmax[i+1]){
			       rightmax[i] = arr[i];
		       }
		       else{
			       rightmax[i] = rightmax[i+1];
		       }
	       }
	       for(int i=0; i<arr.length; i++){
		       System.out.print(rightmax[i]+" ");
	       }

	}
}



			

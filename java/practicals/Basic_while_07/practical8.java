/* WAP to print the cube of first 10 natural numbers */
class practical8{
	public static void main(String[] args){
		int i =1;
		while(i<=10){
			System.out.print(i*i*i+" ");
			i++;
		}
	}
}

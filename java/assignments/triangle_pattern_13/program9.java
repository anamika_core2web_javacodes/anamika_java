import java.io.*;
class program9{
        public static void main(String[] args) throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter the number of rows :");
                int rows = Integer.parseInt(br.readLine());
		int k=(rows*(rows+1))-1;
		for(int i=1; i<=rows; i++){
			for(int j=1; j<=rows-i+1; j++){
				System.out.print(k+" ");
				k=k-2;
			}
			System.out.println();
		}
	}
}

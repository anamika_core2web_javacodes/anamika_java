class switch3{
	public static void main(String[] args){
		String s="XL";
		switch(s){
			case "XL":
				System.out.println("Extra Large");
				break;
			case "S":
				System.out.println("Small");
				break;
			case "XXL":
				System.out.println("Extra Extra Large");
				break;
			case "XS":
				System.out.println("Extra Small");
				break;
			default:
				System.out.println("In default state");
		}
	}
}	

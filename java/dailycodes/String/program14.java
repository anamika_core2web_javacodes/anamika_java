class program14{
        public static void main(String[] args){
                String str1="Anamika";   //in scp
                System.out.println(System.identityHashCode(str1));
                String str2="Anamika";
                //in SCP
                System.out.println(System.identityHashCode(str2));
                String str3= new String("Anamika");
                // Object Type String or new String (heap)
                System.out.println(System.identityHashCode(str3));
                String str4= new String("Anamika");
                // Object Type String or new String (heap)
                System.out.println(System.identityHashCode(str4));
        }
}

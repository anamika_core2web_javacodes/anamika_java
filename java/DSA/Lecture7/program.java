/*
 Array Rotation :
 - Given an integer Array A of size N and an Integer B, you have to return the same array after rotating it B times towards Right

*/

class Demo{
	public static void main(String[] args){
		int arr1[] = new int[]{1,2,3,4};
		int arr2[] = new int[arr1.length];
		int x=2;
		int k=0;
		for(int i=x; i<arr1.length; i++){
			arr2[k] = arr1[i];
			k++;
		}
		for(int i=0; i<x; i++){
			arr2[k] = arr1[i];
			k++;
		}
		for(int i=0; i<arr1.length; i++){
			arr1[i] = arr2[i];
		}
		for(int i=0; i<arr1.length; i++){
			System.out.print(arr1[i]+" ");
		}
	}
}


/* WAP to print the square of first 10 natural numbers in reverse manner */
class practical4{
	public static void main(String[] args){
		int i=10; 
		while(i>=1){
			System.out.print(i*i+" ");
			i--;
		}
		System.out.println();
	}
}

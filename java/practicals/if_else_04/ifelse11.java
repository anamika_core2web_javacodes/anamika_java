//WAP to check whether the given number is in the range of 1 to 1000 or not.

class ifelse11{
	public static void main(String[] args){
		int num=-90;
		if(num>=1 && num<=1000)
		{
			System.out.println(num+" is in the range of 1 to 1000");
		}
		else{
			System.out.println(num+" is not in the range of 1 to 1000");
		}
	}
}

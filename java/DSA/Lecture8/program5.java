/* Given an character array (Lowercase)
 * Return the count of (i,j) such that
   a. i<j
   b. arr[i] = 'a';
      arr[j] = "g";
   arr : [a,b,e,g,a,g]
   output : 3
   Bruteforce approach
   */
class program5{
	public static void main(String[] args){
		 char arr[] =new char[]{'a','b','e','g','a','g'};
		 int count = 0;
		for(int i=0; i<arr.length; i++){
			for(int j=i+1; j<arr.length; j++){
				if(arr[i] == 'a'){
					if(arr[j] == 'g'){
						count++;
					}
				}
			}

		}
		System.out.println(count);

	}
}






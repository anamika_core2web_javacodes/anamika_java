import java.io.*;
class program6{
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the number of rows :");
		int rows = Integer.parseInt(br.readLine());
		for(int i=1; i<=rows; i++){
			int c=1;
			int k=1;
			char ch='a';
			for(int j=1; j<=rows-i+1; j++){
				if(k%2!=0){
					System.out.print(c+" ");
					c++;
				}
				else{
					System.out.print(ch+" ");
					ch++;
				}
				k++;
			}
			System.out.println();
		}
	}
}




//WAP to take 2 numbers from the user and return the count of numbers between them
//Bruteforce Approach
import java.io.*;
class program3{
	
		static int numbers(int num1, int num2){
			int count = 0;
			for(int i=num1; i<=num2; i++){
				count++;
			}
			return count;
		}
		public static void main(String[] argts)throws IOException{

			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			System.out.println("Enter the 2 numbers :");
			int num1 =Integer.parseInt(br.readLine());
			int num2 = Integer.parseInt(br.readLine());
			int count = numbers(num1,num2);
			System.out.println("The count of the numbers is "+count);
		}
}



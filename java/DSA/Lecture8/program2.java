/* Given an integer array of size N
 Build an array leftmax of size N
 Leftmax i contains the maximum index 0 to index i;
Arr : [-3,6,2,4,5,2,8,-9,3,1]
N : 10;
leftmax : [-3,6,6,6,6,6,8,8,8,8]

*/
class program2{
	public static void main(String[] args){
		int arr[] = new int[]{-3,6,2,4,5,2,8,-9,3,1};
		int leftmax[] =new int[arr.length];
		int max = Integer.MIN_VALUE;
		for(int i=0; i<arr.length; i++){
			for(int j=0; j<=i; j++){
				if(arr[j]>max){
					max = arr[j];
				}
			}
			leftmax[i] = max;
		}
		for(int i=0; i<arr.length; i++){
			System.out.print(leftmax[i]+" ");
		}
	}
}

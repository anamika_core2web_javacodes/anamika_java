//WAP to print the product of 10 natural numbers
class forloop10{
	public static void main(String[] args){
		int sum = 1;
		for(int i=1; i<=10; i++){
			sum = sum*i;
		}
		System.out.println(sum);
	}
}
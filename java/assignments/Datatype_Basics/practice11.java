/*Write the program to print the value of gravity and print the letter used to represent the acceleration due to gravity*/

class practice11{
	public static void main(String[] args)
	{
		float gravity = 9.8f;
		char letter = 'a';
		System.out.println("Value of gravity :"+gravity);
		System.out.println("Letter used to represent the gravity :"+letter);
	}
}
//Reversing the array using while loop
import java.io.*;
class program5{
	public static void main(String[] args)throws IOException{
		int arr[] = new int[]{8,4,1,3,9,2,6,7};
		int N = arr.length;
		int i=0;
		int j=N-1;
		while(i<j){
			int temp = arr[i] ;
			arr[i] = arr[j];
			arr[j] = temp;
			i++; 
			j--;
		}
	
		for(int k=0; k<arr.length; k++){

			System.out.print(arr[k]+" ");
		}
	}

}

/* Write the program to print the numbers divisible by 5 in range of 50-10 */
class practical2{
	public static void main(String[] args){
		int i=50;
		while(i>=10){
			if(i%5==0){
				System.out.print(i+" ");
			}
			i--;
		}
	}
}

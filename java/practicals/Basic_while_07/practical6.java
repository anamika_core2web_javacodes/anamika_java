/* WAP to to print only consonants in from A to Z ,but there must not be any vowel int the output */
class practical6{
	public static void main(String[] args){
		char ch ='A';
		while(ch<='Z'){
			if(ch=='A' || ch=='E' || ch=='I' || ch=='O' || ch=='U'){
			       continue;
			}
	 		else{
				System.out.print(ch+" ");
			}
			ch++;
		}
	}
}	

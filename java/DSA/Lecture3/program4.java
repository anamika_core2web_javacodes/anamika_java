//Optimized code for previous code 
import java.io.*;
class program4{
	static int numbers(int n1,int n2)
	{
		int count = (n2-n1)+1;
		return count;
	}
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the numbers :");
		int n1 = Integer.parseInt(br.readLine());
		int n2 = Integer.parseInt(br.readLine());
		int count = numbers(n1,n2);
		System.out.println(count);
	}
}


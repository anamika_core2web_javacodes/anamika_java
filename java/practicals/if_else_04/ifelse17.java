//Calculate the profit and loss 
//WAP that takes cost price and selling price and calculate its profit and loss

class ifelse17{
	public static void main(String[] args){
		int sell_price = 90;
		int cost_price = 90;
		int profit,loss;
		if(sell_price > cost_price)
		{
			profit = sell_price - cost_price;
			System.out.println("Profit of "+profit);
		}
		else if(cost_price > sell_price){
			loss = cost_price - sell_price;
			System.out.println("Loss of "+loss);
		}
		else
		{
			System.out.println("no profit no loss");
		}
	}
}
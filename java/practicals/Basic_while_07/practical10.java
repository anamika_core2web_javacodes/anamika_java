/* WAP to print the numbers in range 100-24 which are divisible by 4 and 5 */
class practical10{
	public static void main(String[] args){
		int i=100;
		while(i>=24){
			if(i%4==0 && i%5==0){
				System.out.print(i+" ");
			}
			i--;
		}
	}
}

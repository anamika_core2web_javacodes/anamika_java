//Swap number code : space complexity:O(1);
import java.io.*;
class program5{
	static void swapno(int num1,int num2){
		int temp=0;
		temp=num1;
		num1=num2;
		num2=temp;
		System.out.println("The swapped numbers are "+"\n"+"num1 = "+num1+"\nnum2 = "+num2);

	}
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the numbers :");
		int num1 = Integer.parseInt(br.readLine());
		int num2 = Integer.parseInt(br.readLine());
		System.out.println("The number before swapping :"+"\n"+num1+" "+num2);
		swapno(num1,num2);
	}
}






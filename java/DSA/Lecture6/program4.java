/*Given the array of size N
 Return the count of pairs (i,j) with arr[i]+arr[j]=k;
arr : [3,5,2,1,-3,7,8,15,6,13];
N : 10
K : 10
output : 6
*/
class program4{
	public static void main(String[] args){
		int arr[] = new int[]{3,5,2,1,-3,7,8,15,6,13};
		int count = 0;
		int k=10;
		for(int i=0; i<arr.length; i++){
			for(int j=i+1; j<arr.length; j++){
				if(arr[i]+arr[j]==k && i!=j){
					count++;
				}
			}
		}
		System.out.println(count*2);

	}
}
//Time complexity : O(N^2);
//Space complexity : O(1)



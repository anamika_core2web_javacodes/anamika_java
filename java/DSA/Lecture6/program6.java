/*Given an array of size N
 Find the second largest element in an array
*/
class program6{
	public static void main(String[] args){
		int arr[] = new int[]{8,4,1,3,9,2,6,7};
		int max = Integer.MIN_VALUE;
		for(int i=0; i<arr.length; i++){
			if(arr[i]> max){
				max = arr[i];
			}
		}
		int max2 = Integer.MIN_VALUE;
		for(int i=0; i<arr.length; i++){
			if(arr[i]>max2 && arr[i]<max){
				max2 = arr[i];
			}
		}
		System.out.println(max2);
	}
}




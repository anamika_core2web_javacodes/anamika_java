class if3{
	public static void main(String[] args){
		String s="S";
		if(s.equals("XL")){
			System.out.println("Extra Large");
		}else if(s.equals("XS")){
			System.out.println("Extra Small");
		}else if(s.equals("S")){
			System.out.println("Small");
		}else if(s.equals("XXL")){
			System.out.println("Extra Extra Large");
		}
		else{
			System.out.println("Invalid Size");
		}
	}
} 

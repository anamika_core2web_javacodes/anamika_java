import java.io.*;
class program3{
        public static void main(String[] args)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter number of rows :");
                int rows = Integer.parseInt(br.readLine());
		int num=3;
		for(int i=1; i<=rows; i++){
			for(int sp=1; sp<=rows-i; sp++){
				System.out.print(" ");
			}
			for(int j=1; j<=2*i-1; j++){
				System.out.print(num+" ");
			}
			num--;
			System.out.println();
		}
	}
}

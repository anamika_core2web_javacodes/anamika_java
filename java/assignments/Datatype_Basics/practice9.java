/*Write a program to print the temperature of the air conditioner in degrees and also print the standard room temperature using 
appropriate data types. */

class practice9{
	public static void main(String[] args){
		float temp_airconditioner = 36.90f;
		float standard_room_temperature = 27f;
		System.out.println("Temperature of air conditioner :"+temp_airconditioner);
		System.out.println("Standard room temperature :"+standard_room_temperature);
	}
}
		 	
import java.io.*;
class program9{
        public static void main(String[] args)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter number of rows :");
                int rows = Integer.parseInt(br.readLine());
		for(int i=1; i<=rows; i++){
			for(int sp=1; sp<=rows-i; sp++){
				System.out.print(" ");
			}
			char ch1='a';
			char ch2='A';
			for(int j=1; j<=2*i-1; j++){
				if(i%2==0){
					if(j<i){
						System.out.print(ch1+" ");
						ch1++;
					}
					else{
						System.out.print(ch1+" ");
						ch1--;
					}
				}
				else{
					if(j<i){
						System.out.print(ch2+" ");
						ch2++;
					}
					else{
						System.out.print(ch2+" ");
						ch2--;
					}
				}
			}
			System.out.println();
		}
	}
}




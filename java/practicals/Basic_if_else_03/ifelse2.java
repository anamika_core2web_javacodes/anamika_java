//Write a program to check whether the given number is even or odd.

class ifelse2{
	public static void main(String[] args)
	{
		int num = 5;
		if(num % 2 ==0)
		{
			System.out.println("The number is even");
		}
		else if(num % 2 != 0)
		{
			System.out.println("The number is odd");
		}
		else{
			System.out.println("The number is invalid");
		}
	}
}

		
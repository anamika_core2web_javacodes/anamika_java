//Write a program to check whether the number id divisible by 7 or not.

class ifelse5{
	public static void main(String[] args){
		int num=105;
		if(num % 7 == 0){
			System.out.println("The number is divisible by 7");
		}
		else{
			System.out.println("The number is not divisible by 7");
		}
	}
}
		